// Dropdown Menu
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function dropDownFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
    if (!e.target.matches('.dropbtn')) {
        const myDropdown = document.getElementById("myDropdown");
        if (myDropdown.classList.contains('show')) {
            myDropdown.classList.remove('show');
        }
    }
};

// Get the modal
const modal = document.getElementById("myModal");

// Get the button that opens the modal
const btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
const span = document.getElementsByClassName("close")[0];

const form = document.getElementsByClassName('form')[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.classList.toggle('active');
};

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.classList.toggle('active');
    document.querySelector(".input-container.active").classList.remove("active");
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
};

class DoctorVisit{
    constructor(name, lastName, visitPurpose, visitDate, textArea){
        this.name = name;
        this.lastName = lastName;
        this.visitPurpose = visitPurpose;
        this.visitDate = visitDate;
        this.textArea = textArea;
    }
    getName(){
        return this.name;
    }
    getLastName(){
        return this.lastName;
    }
    getVisitPurpose(){
        return this.visitPurpose;
    }
    getVisitDate(){
        return this.visitDate;
    }
}

class CardioVisit extends DoctorVisit{
    constructor(name, lastName, visitPurpose, visitDate, age, bloodPressure,hartDiseaseRecord, weightIndex, textArea){
        super(name, lastName, visitPurpose, visitDate, textArea);
        this.age = age;
        this.bloodPressure = bloodPressure;
        this.hartDiseaseRecord = hartDiseaseRecord;
        this.weightIndex = weightIndex;
    }
    getAge(){
        return this.age;
    }
    getBloodPressure(){
        return this.bloodPressure;
    }
    getHartDiseaseRecord(){
        this.hartDiseaseRecord = this.hartDiseaseRecord.split(',');
        return this.hartDiseaseRecord;
    }
    getWeightIndex(){
        return this.weightIndex;
    }
    render(){
        const modal = document.getElementsByClassName('modal')[0];
        modal.classList.toggle("active");

        //создание уникального ключа для карточки
        let key = new Date().toISOString();
        let cardHTML = `<div data-key="${key}" id="visitContainer" class="visit-container visit-container-cardio">
                <button id="btnCloseCard" class="btn-close-card" onclick = "this.parentElement.remove(); localStorage.removeItem('${key}')">X</button>
                <p class="card-text">NAME: ${this.name}</p>
                <p class="card-text">LAST NAME: ${this.lastName}</p>
                <p class="card-text none additional" >AGE: ${this.age}</p>
                <p class="card-text none additional" >VISIT PURPOSE: ${this.visitPurpose}</p>
                <p class="card-text none additional" >VISIT DATE: ${this.visitDate}</p>
                <p class="card-text none additional" >BLOOD PRESSURE: ${this.bloodPressure}</p>
                <p class="card-text none additional" >HART DISEASE RECORD:  ${this.hartDiseaseRecord}</p>
                <p class="card-text none additional" >WEIGHT INDEX: ${this.weightIndex}</p>
                <textarea maxlength="400" class="textarea">${this.textArea}</textarea>
                <button id="btnShow" class="btn-show">SHOW MORE</button>
                </div>`;
        form.innerHTML += cardHTML;
        findAllAndDrag();
        localStorage.setItem(`${key}`,cardHTML);
        clearModalInputs();
    }
    showMore(){
    }
}

const cardio = document.getElementById("cardio");
cardio.addEventListener("click", function () {
    document.getElementById("myDropdown").classList.remove("show");
    let container = document.getElementById('inputContainerCardio');
    if (document.querySelector(".input-container.active")) {
        document.querySelector(".input-container.active").classList.remove("active");
    }
    container.classList.add("active");

    if(container.getAttribute('data-flag')){
       }else{

        container.innerHTML = `<input  id="name" class="name-visit clear" type="text" required placeholder="NAME">
        <input id="lastName" class="last-name clear" type="text" required placeholder="LAST NAME">
        <input id="age" class="age clear" type="text" required placeholder="AGE">
        <input id="visitPurpose" class="visit-purpose clear" type="text" required placeholder="VISIT PURPOSE">
        <input id="visitDate" class="visit-date clear" type="text" required placeholder="VISIT DATE">
        <input id="bloodPressure" class="blood-pressure clear" type="text" required placeholder="BLOOD PRESSURE">
        <input id="hartDiseaseRecord" class="hart-disease-record clear" type="text" required placeholder="HART DISEASE RECORD">
        <input id="weightIndex" class="weight-index clear" type="text" required placeholder="WEIGHT INDEX">
        <textarea id="textArea" maxlength="400" class="textarea clear" placeholder="Enter text"></textarea>`;
        container.setAttribute('data-flag','true');

    }

});

const mainBtn = document.getElementById('cardCreate');

mainBtn.addEventListener('click',function(){
    const docActive = document.querySelector(".input-container.active");
    docActive.classList.remove("active");
    const textForm = document.querySelector(".form-text");
    textForm.classList.add('none');

    if(docActive.id === 'inputContainerCardio'){
        CardioStart();

    }if(docActive.id === 'inputContainerStomat'){
        StomatStart();

    }if(docActive.id === 'inputContainerTherapist') {
        TherapistStart();
    }
});

function CardioStart() {
    let cardio1 = new CardioVisit(
        document.getElementById('name').value,
        document.getElementById('lastName').value,
        document.getElementById('age').value,
        document.getElementById('visitPurpose').value,
        document.getElementById('visitDate').value,
        document.getElementById('bloodPressure').value,
        document.getElementById('hartDiseaseRecord').value,
        document.getElementById('weightIndex').value,
        document.getElementById('textArea').value
    );
    cardio1.render();
}

class StomatoVisit extends DoctorVisit{
    constructor(name, lastName, visitPurpose, visitDate, lastVisitDate, textArea){
        super(name, lastName, visitPurpose, visitDate, textArea);
        this.lastVisitDate = lastVisitDate;
    }

    getLastVisitDate(){
        return this.lastVisitDate;
    }

    render(){
        const modal = document.getElementsByClassName('modal')[0];
        modal.classList.toggle('active');
        let key = new Date().toISOString();
        let cardHTML = `<div data-key="${key}" id="visitContainer" class="visit-container visit-container-stomat">
                <button id="btnCloseCard" class="btn-close-card" onclick = "this.parentElement.remove(); localStorage.removeItem('${key}')">X</button>
                <p class="card-text">NAME: ${this.name}</p>
                <p class="card-text">LAST NAME: ${this.lastName}</p>
                <p class="card-text none additional">VISIT PURPOSE: ${this.visitPurpose}</p>
                <p class="card-text none additional">VISIT DATE: ${this.visitDate}</p>
                <p class="card-text none additional">LAST VISIT DATE: ${this.lastVisitDate}</p>
                <textarea maxlength="400" class="textarea">${this.textArea}</textarea>
                <button id="btnShow" class="btn-show">SHOW MORE</button>
                </div>`;

        form.innerHTML += cardHTML;
        findAllAndDrag();
        localStorage.setItem(`${key}`,cardHTML);
        clearModalInputs();
    }
    showMore(){
    }
}

const stomat = document.getElementById('stomat');

stomat.addEventListener("click", function () {
    document.getElementById("myDropdown").classList.remove("show");
    let container = document.getElementById('inputContainerStomat');
    if (document.querySelector(".input-container.active")) {
        document.querySelector(".input-container.active").classList.remove("active");
    }
    container.classList.add("active");
    if(container.getAttribute('data-flag')){
    }else {
        container.innerHTML = `<input id="nameS" class="name-visit clear " type="text" required placeholder="NAME">
            <input id="lastNameS" class="last-name clear" type="text" required placeholder="LAST NAME">
            <input id="visitPurposeS" class="visit-purpose clear" type="text" required placeholder="VISIT PURPOSE">
            <input id="visitDateS" class="visit-date clear" type="text" required placeholder="VISIT DATE">
            <input id="lastVisitDateS" class="last-visit-date clear" type="text" required placeholder="LAST VISIT DATE">
            <textarea id="textAreaS" maxlength="400" class="textarea clear" placeholder="Enter text"></textarea>`;
        container.setAttribute('data-flag','true');
    }
});

function StomatStart() {
    let stomato1 = new StomatoVisit(
        document.getElementById('nameS').value,
        document.getElementById('lastNameS').value,
        document.getElementById('visitPurposeS').value,
        document.getElementById('visitDateS').value,
        document.getElementById('lastVisitDateS').value,
        document.getElementById('textAreaS').value
    );
    stomato1.render();
}

class TherapistVisit extends DoctorVisit{
    constructor(name, lastName, visitPurpose, visitDate, age, textArea){
        super(name, lastName, visitPurpose, visitDate, textArea);
        this.age = age;
    }
    getAge(){
        return this.age;
    }
    render(){
        const modal = document.getElementsByClassName('modal')[0];
        modal.classList.toggle('active');
        let key = new Date().toISOString();
        let cardHTML = `<div data-key="${key}" id="visitContainer" class="visit-container visit-container-therapist">
                <button id="btnCloseCard" class="btn-close-card" onclick = "this.parentElement.remove(); localStorage.removeItem('${key}')">X</button>
                <p class="card-text">NAME: ${this.name}</p>
                <p class="card-text">LAST NAME: ${this.lastName}</p>
                <p class="card-text none additional">VISIT PURPOSE: ${this.visitPurpose}</p>
                <p class="card-text none additional">VISIT DATE: ${this.visitDate}</p>
                <p class="card-text none additional">AGE: ${this.age}</p>
                <textarea maxlength="400" class="textarea">${this.textArea}</textarea>
                <button id="btnShow" class="btn-show">SHOW MORE</button>
                </div>`;
        form.innerHTML += cardHTML;
        findAllAndDrag();
        localStorage.setItem(`${key}`,cardHTML);
        clearModalInputs();
    }
    showMore(){
    }
}

const therapist = document.getElementById('therapist');

therapist.addEventListener("click", function () {
    document.getElementById("myDropdown").classList.remove("show");
    let container = document.getElementById('inputContainerTherapist');
    if (document.querySelector(".input-container.active")) {
        document.querySelector(".input-container.active").classList.remove("active");
    }
    container.classList.add("active");
    if(container.getAttribute('data-flag')){

    }else {

        container.innerHTML = `<input id="nameT" class="name-visit clear" type="text" required placeholder="NAME">
            <input id="lastNameT" class="last-name clear" type="text" required placeholder="LAST NAME">
            <input id="visitPurposeT" class="visit-purpose clear" type="text" required placeholder="VISIT PURPOSE">
            <input id="visitDateT" class="visit-date clear" type="text" required placeholder="VISIT DATE">
            <input id="ageT" class="age clear" type="text" required placeholder="AGE">
            <textarea id="textAreaT" maxlength="400" class="textarea clear" placeholder="Enter text"></textarea>`;
        container.setAttribute('data-flag','true');
    }
});

function TherapistStart() {
    let therapistNew = new TherapistVisit(
        document.getElementById('nameT').value,
        document.getElementById('lastNameT').value,
        document.getElementById('visitPurposeT').value,
        document.getElementById('visitDateT').value,
        document.getElementById('ageT').value,
        document.getElementById('textAreaT').value
    );
    therapistNew.render();
}

document.addEventListener('click', function (event) {
    if (event.target.classList.contains('btn-show')) {
        let parent = event.target.parentElement;
        let hiddenPar = parent.querySelectorAll('.additional');
        hiddenPar.forEach((item) => {
            item.classList.toggle('none');
        });
    }
});

function dataLocal() {
    if(localStorage.length){
        let length = localStorage.length;
        for(let i =0; i<length;i++){
            let key = localStorage.key(i);
            let info = localStorage.getItem(key);
            form.innerHTML+=info;
        }
    }
}
dataLocal();

function clearModalInputs() {
    const allForms = document.querySelectorAll('.clear');
    for(let i =0; i < allForms.length; i++){
        allForms[i].value = '';
    }
}

const textForm = document.querySelector(".form-text");
if (localStorage.length !==0) {
    textForm.classList.add('none');
}

document.getElementById('formCard').onclick = function (event) {

    if(event.target.attributes.class.value.split(' ').includes('visit-container')) {
        event.target.addEventListener('click', function (elem) {
            if (elem.target.className === 'cross') {

            }
        });

    }

};

function makeCardDraggable(item) {
    let card = item;
    function move(e) {

        card.style.zIndex=1;
        let cord = card.getBoundingClientRect();
        let dek = form.getBoundingClientRect();
        if ((cord.x - 20 - dek.x) < 0) {
            card.mousePositionX = e.clientX + card.offsetLeft - 40;
        }
        if ((cord.y - 20 - dek.y) < 0) {
            card.mousePositionY = e.clientY -20;
        }
        if (((dek.x + dek.width) - (cord.x + cord.width + 20)) < 0) {
            card.mousePositionX = (card.offsetLeft + cord.width - dek.width) + e.clientX + 30;
        }
        if (((dek.y + dek.height) - (cord.y + cord.height + 20)) < 0) {
            card.mousePositionY = ( cord.height - dek.height) + e.clientY + 30;
        }
        card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
    }
    card.addEventListener('mousedown', (e) => {

        if (card.style.transform) {
            const transforms = card.style.transform;
            const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
            const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
            card.mousePositionX = e.clientX - transformX;
            card.mousePositionY = e.clientY - transformY;
        } else {
            card.mousePositionX = e.clientX;
            card.mousePositionY = e.clientY;
        }
        document.addEventListener('mousemove', move);

    });
    document.addEventListener('mouseup', e => {
        card.style.zIndex='initial';

        // let card = e.target.closest('.visit-container');
        // if(card){
        //     let id = card.getAttribute('data-key');
        //     localStorage.removeItem(id);
        //     localStorage.setItem(id,card.outerHTML);
        //     console.log(id);

            document.removeEventListener('mousemove', move);
        // }
        // return false;
    });
}

function findAllAndDrag() {
    if(document.querySelectorAll('.visit-container').length){
        let allCards = document.querySelectorAll('.visit-container');
        allCards.forEach((item)=>{
            makeCardDraggable(item)
        })
    }
}

findAllAndDrag();



